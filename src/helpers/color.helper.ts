export function getRandomColor(): string {
    // Generate random values for red, green, and blue channels
    const red: number = Math.floor(Math.random() * 256);
    const green: number = Math.floor(Math.random() * 256);
    const blue: number = Math.floor(Math.random() * 256);
  
    // Construct the color string in hexadecimal format
    const color: string = `#${red.toString(16)}${green.toString(16)}${blue.toString(16)}`;
  
    return color;
  }