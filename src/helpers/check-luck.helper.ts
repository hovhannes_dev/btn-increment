export default function isLucky(percent = 5): boolean {
    const rand = Math.floor(Math.random() * 101); // Generates a random number between 0 and 100 (inclusive)

    return rand <= 5;
}