import { useRef, useCallback } from 'react';

const useDebounce = <T extends (...args: any[]) => void>(fn: T, delay: number = 10000, dependencies: any[] = []): T => {
  const timerId = useRef<NodeJS.Timeout | null>(null);


  const debounce = useCallback((...args: any[]) => {
    if (timerId.current) {
      clearTimeout(timerId.current);
    }
    timerId.current = setTimeout(() => {
      fn(...args);
      timerId.current = null;
    }, delay);
  }, dependencies)

  return debounce as T;
};

export default useDebounce;