import { useRef, useCallback } from 'react';

const useThrottle = <T extends (...args: any[]) => void>(
  fn: T,
  trailingCount: number = 3,
  wait: number = 1000,
  lead: boolean = true,
): T => {
  const timerId = useRef<NodeJS.Timeout | null>(null);
  const lastArgs = useRef<any[] | null>();
  const trolledCount = useRef<number>(0);

  const throttle = useCallback(

    function (...args: any[]) {
      const waitFunc = () => {
        // if trailing invoke the function and start the timer again and not reached max count for lead start
        if (lastArgs.current && trolledCount.current < trailingCount) {
          fn(lastArgs.current);
          lastArgs.current = null;
          timerId.current = setTimeout(waitFunc, wait);
          trolledCount.current++;
        } else {
          // else reset the timer
          timerId.current = null;
          trolledCount.current = 0;
        }
      };

      // if leading run it right away
      if (!timerId.current && lead) {
        fn(args);
        trolledCount.current++;
      }
      // else store the args
      else {
        lastArgs.current = args;
      }

      // run the delayed call while allowed
      if (!timerId.current && trolledCount.current >= trailingCount && lead) {
        timerId.current = setTimeout(waitFunc, wait);
      }

      // if not lead start only with timer
      if(!lead) {
        timerId.current = setTimeout(waitFunc, wait);
      }
    },
    [fn, wait, trailingCount]
  );

  return throttle as T;
};

export default useThrottle;