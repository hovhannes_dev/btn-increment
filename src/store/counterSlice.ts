import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './store';
import { getRandomColor } from '../helpers/color.helper';

export interface CounterState {
  value: number;
  color: string;
  overdriveTimeout: number;
  trophies: number[];
}

const initialState: CounterState = {
  value: 0,
  color: '#ff0',
  overdriveTimeout: 0,
  trophies: []
};

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state, action: PayloadAction<number>) => {
      state.value += action.payload;
    },
    decrement: (state, action: PayloadAction<number>) => {
      state.value -= action.payload;
    },
    changeColor: (state) => {
        state.color = getRandomColor();
    },
    setOverdriveTimeout: (state, action: PayloadAction<number>) => {
        state.overdriveTimeout = action.payload;
    },
    addTrophy: (state, action: PayloadAction<number>) => {
        state.trophies.push(action.payload);
    }
  },
});

export const { increment, decrement, changeColor, setOverdriveTimeout, addTrophy } = counterSlice.actions;

export const selectCount = (state: RootState) => state.counter.value;
export const selectColor = (state: RootState) => state.counter.color;
export const selectTrophies = (state: RootState) => state.counter.trophies;
export const selectOverdriveTimeout = (state: RootState) => state.counter.overdriveTimeout;

export default counterSlice.reducer;
