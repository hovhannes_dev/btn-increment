import { useRef, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    increment,
    decrement,
    selectCount,
    selectColor,
    changeColor,
    selectOverdriveTimeout,
    setOverdriveTimeout,
    selectTrophies,
    addTrophy
} from '../store/counterSlice';
import { Row, Col, Button, Typography } from 'antd';
import useThrottle from '../hooks/useThrottle';
import isLucky from '../helpers/check-luck.helper';
import useDebounce from '../hooks/useDebounce';

const { Title } = Typography;


let incrementCount = 1;

export function Counter() {
    const count = useSelector(selectCount);
    const countRef = useRef<number>(count);
    const color = useSelector(selectColor);
    const trophies = useSelector(selectTrophies);
    const overdriveTimeout = useSelector(selectOverdriveTimeout);
    const overdriveRef = useRef<number>(overdriveTimeout);
    const idleIntervalId = useRef<NodeJS.Timeout>();
    const ovrdrvIntervalId = useRef<NodeJS.Timeout>();

    const dispatch = useDispatch();
    const incrementOnClick = useThrottle(incrementHandler);

    const idleIntervalHandler = useCallback(() => {
        {
            dispatch(changeColor());
            if(idleIntervalId.current) {
                clearInterval(idleIntervalId.current);
            }
            
            if (countRef.current > 0) {
                dispatch(decrement(1));
                idleIntervalId.current = setInterval(() => {
                    if (countRef.current > 0) {
                        dispatch(decrement(1));
                    } else {
                        clearInterval(idleIntervalId.current);
                    }
                }, 1000);
            }
        }
    }, []);

    const overdriveThorttle = useThrottle(() => incrementCount = 1, 1, 10000, false);
    const idleThrottler = useDebounce(idleIntervalHandler, 10000, []);

    useEffect(() => {
        if(count > countRef.current) {
            idleThrottler();
        }

        // in case of you reached to 9 and got overdrive. Next move is 11 and 10 needs to be passed
        let prevNumber = incrementCount === 2 ? count - 1 : 0;
        pushToTrophies(prevNumber);
        pushToTrophies(count);

        countRef.current = count;
    }, [count]);

    useEffect(() => {
        overdriveRef.current = overdriveTimeout;
    }, [overdriveTimeout]);

    function pushToTrophies(num: number) {
        if(num > 0 && num % 10 === 0 && !trophies.includes(num)) {
            dispatch(addTrophy(num));
        }
    }

    
    function incrementHandler(): void {
        if (isLucky()) {
            incrementCount = 2;
            overdriveThorttle();
            startOverdrive();
            console.log('Lucky case');
        }
        
        dispatch(increment(incrementCount));
    }

    function onClick(): void {
        incrementOnClick();
        clearInterval(idleIntervalId.current);
    }

    const startOverdrive = useCallback(() => {
        dispatch(setOverdriveTimeout(10));
        if(ovrdrvIntervalId.current) {
            clearInterval(ovrdrvIntervalId.current)
        }

        ovrdrvIntervalId.current = setInterval(() => {
            dispatch(setOverdriveTimeout(overdriveRef.current - 1));
        }, 1000);
    }, [])

    return (
        <>
            <Row align="middle" justify="center">
                <Col span={4} offset={2}>
                    <Title>{count}</Title>
                    <div>
                        <Button onClick={onClick} style={{ background: color }}>
                            Increment
                        </Button>
                    </div>
                    { overdriveTimeout > 0 && <Title level={4}>Overdrive will finish in {overdriveTimeout} seconds</Title> }
                    
                </Col>
                <Col span={8} offset={1}>
                    { trophies.map(trophy => (
                        <Title key={trophy} level={3}>You earned trophy for reaching {trophy}!!!</Title>
                    )) }
                </Col>
            </Row>
        </>

    );
}